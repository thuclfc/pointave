/*!
 * Odyssey
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2020. MIT licensed.
 *//**
 * Created by Thuc-lfc on 3/12/2019.
 */
$(document).ready(function () {
    $('.navbar-toggler').click(function () {
        $('header nav').toggleClass('active');
        $('.modal-menu').addClass('is-open');
    });
    $('.navbar-collapse .close,.modal-menu').click(function () {
        $(".navbar-collapse").collapse('hide');
        $('.navbar').removeClass('active');
        $('.modal-menu').removeClass('is-open');
    });

    // active navbar of page current
    var urlcurrent = window.location.href;
    $(".navbar-nav li a[href$='" + urlcurrent + "']").addClass('active');

    // effect navbar
    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('.navbar').addClass('scroll');
        } else {
            $('.navbar').removeClass('scroll');
        }
    });
    $(window).on("load", function (e) {
        $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'><i class='fas fa-sort-down'></i></span>");
        $('.loading').hide();
    });
    $('.navbar-nav > li').click(function () {
        $('.navbar-nav > li').removeClass('active');
        $(this).toggleClass('active');
    });
});