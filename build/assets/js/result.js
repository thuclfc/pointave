/*!
 * Odyssey
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2020. MIT licensed.
 */$(document).ready(function () {
    // js slide logo
    $('.slider-logo').slick({
        centerMode: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        centerPadding: 0,
        dots: false,
        arrows: true,
        responsive: [{
            breakpoint: 480,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 375,
            settings: {
                slidesToShow: 3
            }
        }]
    });

    // slider text
    $('.slider-text').slick({
        centerMode: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        centerPadding: 0,
        dots: false,
        arrows: true,
        responsive: [{
            breakpoint: 1440,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
        }]
    });
});