/**
 * Created by Thuc-lfc on 3/12/2019.
 */
$(document).ready(function () {
    // js effect_border
    $('.effect_border').click(function () {
        $(this).toggleClass('focus');
    });
    $('.effect_border').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            $('.effect_border').removeClass('focus');
            $(this).next().addClass('focus');
        }
    });
    var item_input = $('.effect_border .form-control');
    $(item_input).on('change', function () {
        if ($(this).val().length > 0) {
            $(this).parent().addClass('filled');
        } else {
            $(this).parent().removeClass('filled');
        }
    });

    $('.volume_control .btn-control').click(function () {
        if ($("video").prop('muted')) {
            $("video").prop('muted', false);
        } else {
            $("video").prop('muted', true);
        }
        $(this).toggleClass('on_volume');
    });

    // control autovideo play
    var slideWrapper = $(".slider-for");

    // POST commands to YouTube or Vimeo API
    function postMessageToPlayer(player, command){
        if (player == null || command == null) return;
        player.contentWindow.postMessage(JSON.stringify(command), "*");
    }

    // When the slide is changing
    function playPauseVideo(slick, control){
        var currentSlide, slideType, startTime, player, video;

        currentSlide = slick.find(".slick-current");
        slideType = currentSlide.attr("class").split(" ")[1];
        player = currentSlide.find("iframe").get(0);

        if (slideType === "youtube") {
            switch (control) {
                case "play":
                    postMessageToPlayer(player, {
                        "event": "command",
                        "func": "mute"
                    });
                    postMessageToPlayer(player, {
                        "event": "command",
                        "func": "playVideo"
                    });
                    break;
                case "pause":
                    postMessageToPlayer(player, {
                        "event": "command",
                        "func": "pauseVideo"
                    });
                    break;
            }
        } else if (slideType === "video") {
            video = currentSlide.children("video").get(0);
            if (video != null) {
                if (control === "play"){
                    video.play();
                } else {
                    video.pause();
                }
            }
        }
    }

    // DOM Ready
    $(function() {
        // Initialize
        slideWrapper.on("init", function(slick){
            slick = $(slick.currentTarget);
            setTimeout(function(){
                playPauseVideo(slick,"play");
            }, 1000);
        });
        slideWrapper.on("beforeChange", function(event, slick) {
            slick = $(slick.$slider);
            playPauseVideo(slick,"pause");
        });
        slideWrapper.on("afterChange", function(event, slick) {
            slick = $(slick.$slider);
            playPauseVideo(slick,"play");
        });

        //start the slider
        // js slide video
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 20,
            slidesToScroll: 1,
            vertical: true,
            asNavFor: '.slider-for',
            dots: false,
            focusOnSelect: true
        });
    });

    // js slide about
    $('.aboutUs__slider').slick({
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        dots: true,
        arrows: false
    });

    // counter
    function counter() {
        $('.counter').each(function () {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({ countNum: $this.text() }).animate({
                countNum: countTo
            }, {
                duration: 5000,
                easing: 'linear',
                step: function () {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function () {
                    $this.text(this.countNum);
                    //alert('finished');
                }
            });
        });
    }

    var runCounter = true;
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        var page_scroll = $('.results').offset().top;
        if (page_scroll < window.innerHeight + scroll && runCounter) {
            counter();
            runCounter = false;
        }
    });
});
$(document).mouseup(function (e) {
    var form_group = $('wrapper');
    if (!form_group.is(e.target) && form_group.has(e.target).length === 0) {
        $('.effect_border,.select_b').removeClass('focus');
    }
});

/*
 // control autovideo play
 $('.slider-nav .item').click(function () {
 var index_item = $(this).index();
 $('.slider-for').on('afterChange', function(event, slick, currentSlide, nextSlide){
 $('.slider-for video').trigger('pause');
 $('.slider-for video')[index_item].play();
 });
 });
 $('.slider .slick-arrow').click(function () {
 var index_current =  $('.slider-for .slick-current').index();
 $('.slider-for').on('afterChange', function(event, slick, currentSlide, nextSlide){
 $('.slider-for .item video').trigger('pause');
 $('.slider-for').find('.item.slick-active video').trigger('play');;
 });
 });*/
